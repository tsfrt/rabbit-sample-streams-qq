MASTER_OUTPUT_FILE=Overall-run-$(date +%s).dat


  for j in 1 2 3 4 5
  do
    echo "Starting run - $j" >> $MASTER_OUTPUT_FILE

    export COMPOSE_FILE=docker/docker-compose-qq.yml
    docker-compose down && ./gradlew build && docker-compose build

    docker-compose up -d rmq0-qq 
    docker-compose up -d rmq1-qq 
    docker-compose up -d rmq2-qq 

    sleep 60

    docker-compose up -d hl7-0-consumer 
    docker-compose up -d hl7-1-consumer 

    sleep 10

    docker-compose up -d hl7-0-listener 

    OUTPUT_FILE=$(date +%s).dat
    sleep 2
    OUTPUT_FILE2=$(date +%s).dat

    sleep 90

    #pull results
    docker-compose logs hl7-1-consumer | grep HP0607 | cut -d ' ' -f 13 > ${OUTPUT_FILE}
    docker-compose logs hl7-0-consumer | grep HP0013 | cut -d ' ' -f 13 > ${OUTPUT_FILE2}
    echo "run $j - wrote file $OUTPUT_FILE " >> $MASTER_OUTPUT_FILE
    echo "run $j - wrote file $OUTPUT_FILE2 " >> $MASTER_OUTPUT_FILE

    #validate ascending order
    ./verify.sh $OUTPUT_FILE $j $MASTER_OUTPUT_FILE
  done

