

package mil.dha.ahltam.hl7consumer;

import java.util.function.Consumer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.springframework.messaging.MessageHeaders;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;

@SpringBootApplication
public class PartitioningRabbitDemoApplication {

	private static final Logger logger = LoggerFactory.getLogger(PartitioningRabbitDemoApplication.class);

	private static final Map<String, Vector<String>> resultsMap = new ConcurrentHashMap<String, Vector<String>>();

	public static void main(String[] args) {
		SpringApplication.run(PartitioningRabbitDemoApplication.class, args);
	}
	
	@Bean
	public Consumer<Message<String>> listen() {
		return message -> logger.info(message.getPayload() + " received from partition "
				+ message.getHeaders().get(AmqpHeaders.CONSUMER_QUEUE));
	}

}
