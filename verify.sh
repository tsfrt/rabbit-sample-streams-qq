INPUT_FILE=$1
RUN=$2
MASTER_OUT=$3

lastvalue=-1

while read l; do
  value=$(echo $l | cut -d "-" -f 2 | xargs) 
  if [[ $lastvalue -gt $value ]]
     then
     echo "${INPUT_FILE}-run-${RUN}-failure" >> $MASTER_OUT
  fi
  lastvalue=$value
done <$INPUT_FILE
