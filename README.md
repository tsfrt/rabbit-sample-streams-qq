run `./check_order.sh`, will run through 5 iterations of 10k messages over 100 threads.

Overall-run-<1604904798>.dat will log the iterations and print errors if any out of order messages are found.

Note: The test checks one sample site per consumer, the sites have hashed consistently to the same consumer, so relying on that.  If the .dat files do come up empty flip the consumers in the `check_order.sh`