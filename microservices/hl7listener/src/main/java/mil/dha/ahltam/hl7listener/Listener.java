package mil.dha.ahltam.hl7listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

public class Listener implements Runnable {
	ApplicationContext appContext;
	private String site;
	
	private static final Logger LOG = LoggerFactory.getLogger(Application.class);

	public Listener(ApplicationContext appContext, String site) {
		this.appContext = appContext;
		this.site = site;
	}

	@Override
	public void run() {
		Hl7Producer hl7Producer = new Hl7Producer();
		AutowireCapableBeanFactory beanFactory = appContext.getAutowireCapableBeanFactory();
		beanFactory.autowireBean(hl7Producer);

		//Hard coded for loop simulating receiving 100 messages from a site, imagine while loop receiving messages over socket until there is no data left.
		for (int i = 0; i < 100; i++) {
			LOG.debug(site+"-"+i);
			hl7Producer.sendHL7(site, site+"-"+i);
		}

		hl7Producer.closeChannel();
		
	}
}
