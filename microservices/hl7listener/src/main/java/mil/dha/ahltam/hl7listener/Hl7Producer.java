package mil.dha.ahltam.hl7listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.ThreadChannelConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(Hl7Producer.MessageSources.class)
public class Hl7Producer {	
	public interface MessageSources {
		@Output("generate-out-0")
		MessageChannel output();
	}

	private static final Logger LOG = LoggerFactory.getLogger(Application.class);

	@Autowired
	private MessageSources messageSources;
	
	@Autowired
	private ThreadChannelConnectionFactory tccf;
	
	public void sendHL7(String site, String message) {
		LOG.debug("channel - "+messageSources.output());
		messageSources.output().send(MessageBuilder.withPayload(message).setHeader("partitionKey", site).build());		

	}

	public void closeChannel(){
		tccf.closeThreadChannel();
	}

}
