package mil.dha.ahltam.hl7listener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import com.rabbitmq.client.Address;
import com.rabbitmq.client.ConnectionFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.connection.ThreadChannelConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;

@SpringBootApplication
public class Application implements CommandLineRunner {
	private static final Logger LOG = LoggerFactory.getLogger(Application.class);

	@Autowired
	private ApplicationContext appContext;

	public static void main(String arguments[]) {
		SpringApplication.run(Application.class, arguments);
	}

	@Bean
	public AmqpAdmin AmqpAdmin(@Autowired ThreadChannelConnectionFactory connectionFactory) {
		return new RabbitAdmin((org.springframework.amqp.rabbit.connection.ConnectionFactory) connectionFactory);
	}

	@Bean
	ThreadChannelConnectionFactory cf(@Value("${spring.rabbitmq.addresses}") String addresses)
			throws IOException, TimeoutException {
				
		ConnectionFactory rabbitConnectionFactory = new ConnectionFactory();		
		ThreadChannelConnectionFactory tccf  = new ThreadChannelConnectionFactory(rabbitConnectionFactory);
		tccf.setAddresses(addresses);
		return tccf;
	}

	@Override
	public void run(String... args) throws Exception {
		// RabbitMQ takes a while to start
		Thread.sleep(30000);

		// Simulate client, 100 sites
		String sites = "HP0097:HP0808:HP1170l:HP0062:HP0048:HP0067:HP0108:HP0089:HP0060:HP0085:HP0032:N68084:HP0074:HP0036:HP0330:HP0010:HP0112:HP0019:HP0042:HP0006:HP0106:HP0128:HP0129:HP0364:HP0047:HP0093:N00211:N61726:N61564:N68096:HP0310:HP0119:HP0084:HP0110:HP0008:HP0635:HP0131:N00232:HP0083:HP0061:HP0073:HP0637:HP0629:HP0633:HP0607:HP0114:HP0058:N68093:N66095:HP0075:HP0013:HP0248:HP0009:HP0045:HP0077:HP0125:HP0004:HP0326:HP0059:HP0094:HP0639:HP0050:HP0053:HP0617:HP0079:HP0118:HP0078:HP0638:HP0024:HP0038:HP0064:HP0046:HP0616:HP0051:HP0001:HP0057:HP0618:HP0003:HP0029:HP0055:HP0612:HP0090:HP0101:HP0113:HP0624:HP0098:HP0049:HP0096:HP0043:HP0014:HP0052:HP0124:HP0338:HP0018:HP0005:HP0086:HP0117:HP0095:HP0076:HP0622";

		for (String site : sites.split("\\:")) {
			LOG.info("Starting:" + site);
			Listener l = new Listener(appContext, site);
			Thread t = new Thread(l);
			t.start();
		}

		while (true) {
			// Keep running so threads finish, normally socket.accept would be blocking.
		}
	}
}